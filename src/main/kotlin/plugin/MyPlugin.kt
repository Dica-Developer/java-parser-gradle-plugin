package plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.util.GradleVersion

class MyPlugin : Plugin<Project> {

  val currentTestRuntime = when {
    GradleVersion.current() >= GradleVersion.version("3.4") -> "testRuntimeOnly"
    else -> "testRuntime"
  }

  override fun apply(project: Project) {
    with(project) {
      project.plugins.withType(JavaPlugin::class.java) {
        project.tasks.create("createAst", MyPluginTask::class.java)
      }
    }
  }
}
