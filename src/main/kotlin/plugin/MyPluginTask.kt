package plugin

import com.github.javaparser.ParserConfiguration
import com.github.javaparser.StaticJavaParser
import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.Node
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import com.github.javaparser.ast.expr.MethodCallExpr
import com.github.javaparser.ast.expr.NameExpr
import com.github.javaparser.ast.expr.SimpleName
import com.github.javaparser.ast.stmt.BlockStmt
import com.github.javaparser.ast.stmt.ExpressionStmt
import com.github.javaparser.ast.type.ClassOrInterfaceType
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver
import com.github.javaparser.symbolsolver.resolution.typesolvers.ClassLoaderTypeSolver
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver
import org.gradle.api.DefaultTask
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.io.FileReader
import java.lang.UnsupportedOperationException
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*

fun findNodeInCompilationUnitToLineNumberAndColumn(compilationUnit: CompilationUnit, lineNumber: Int, columnNumber: Int): Optional<Node> {
  return findNodeInNodeToLineNumberAndColumn(compilationUnit, lineNumber, columnNumber)
}

fun findNodeInNodeToLineNumberAndColumn(node: Node, lineNumber: Int, columnNumber: Int): Optional<Node> {
  var result: Optional<Node> = Optional.empty()
  for (child in node.childNodes) {
    val possibleNode = findNodeInNodeToLineNumberAndColumn(child, lineNumber, columnNumber)
    if (possibleNode.isPresent) {
      result = possibleNode
    }
  }
  if (!result.isPresent) {
    if (node.range.isPresent) {
      val range = node.range.get()
      if (lineNumber >= range.begin.line && lineNumber <= range.end.line && columnNumber >= range.begin.column && columnNumber <= range.end.column) {
        result = Optional.of(node)
      }
    }
  }
  return result
}

open class MyPluginTask : DefaultTask() {

  @get:Input
  val javaFile: File
    get() = File(project.properties["javaFile"].toString())

  @get:Input
  val line: Int
    get() = Integer.valueOf(project.properties["line"].toString())

  @get:Input
  val column: Int
    get() = Integer.valueOf(project.properties["column"].toString())

  init {
    group = "dev"
    description = "Generate a list of possible completions to the given context."
  }

  private fun getJavaParserFacade(javaFile: File) : Optional<JavaParserFacade> {
    val javaConvention = project.convention.getPlugin(JavaPluginConvention::class.java)
    val main = javaConvention.sourceSets.find {
      val source = it.allSource.sourceDirectories.find {
        //println(it)
        //println(File(it, javaFile.path).exists())
        javaFile.absolutePath.contains(it.absolutePath)
      }
      (null != source)
    }
    if (null != main) {
      val combinedTypeSolver = CombinedTypeSolver()
      combinedTypeSolver.add(ReflectionTypeSolver(true))
      val urls = ArrayList<URL>()
      main.runtimeClasspath.forEach {
        val dependencyFilePath = it.absolutePath
        if (it.exists()) {
          urls.add(File(dependencyFilePath).toURI().toURL())
          //println(dependencyFilePath)
        } else {
          logger.warn("===> not found: " + dependencyFilePath)
        }
      }
      combinedTypeSolver.add(ClassLoaderTypeSolver(URLClassLoader(urls.toArray(arrayOfNulls<URL>(urls.size)))))
      return Optional.of(JavaParserFacade.get(combinedTypeSolver))
    } else {
      logger.error("not able to generate classpath")
      return Optional.empty<JavaParserFacade>()
    }
  }

  @TaskAction
  fun createAst() {
    require(javaFile.exists()) { "Given java file has to exist." }
    require(column > 0) { "Given column needs to be set and greater 0" }
    require(line > 0) { "Given line needs to be set and greater 0" }
    val javaParserFacade : Optional<JavaParserFacade> = getJavaParserFacade(javaFile)
    if (javaParserFacade.isPresent) {
      val compilationUnit: CompilationUnit = StaticJavaParser.parse(javaFile)
      val node = findNodeInCompilationUnitToLineNumberAndColumn(compilationUnit, line, column)
      if (node.isPresent) {
        var it = node.get()
        if (logger.isDebugEnabled) {
          logger.debug(it.toString() + "P" + it.begin.get().line + "x" + it.begin.get().column + "-" + it.end.get().line + "x" + it.end.get().column + ":" + (it.javaClass))
        }
        try {
          while (NameExpr::class.isInstance(it) || BlockStmt::class.isInstance(it) || ExpressionStmt::class.isInstance(it)) {
            if (it.parentNode.isPresent) {
              it = it.parentNode.get()
            } else {
              throw IllegalStateException("No node found that are able to resolve to a type.")
            }
          }

          var className: Optional<String> = Optional.empty()
          if (SimpleName::class.isInstance(it)) {
            val coit: SimpleName = it as SimpleName
            logger.lifecycle(coit.toString() + "<<<<<<<<<<<<<<<<<<<<<<<<<")
            className = Optional.of(javaParserFacade.get().solve(coit).correspondingDeclaration.type.describe())
          } else if (MethodCallExpr::class.isInstance(it)) {
            val coit: MethodCallExpr = it as MethodCallExpr
            className = Optional.of(javaParserFacade.get().solve(coit).correspondingDeclaration.returnType.describe())
          } else if (ClassOrInterfaceType::class.isInstance(it)) {
            val coit: ClassOrInterfaceType = it as ClassOrInterfaceType
            className = Optional.of(javaParserFacade.get().convert(coit, it).describe())
          } else if (ClassOrInterfaceDeclaration::class.isInstance(it)) {
            // we are on an empty line so no type completions
          } else {
            logger.error("not supported type found >>>>> " + it.javaClass + " <<<<<<< " + it)
          }
          if (className.isPresent) {
            logger.lifecycle("Resolved type to: " + className)
            val resolvedType = javaParserFacade.get().typeSolver.solveType(className.get())
            resolvedType.visibleFields.forEach { m -> logger.lifecycle("  f:" + m.name) }
            resolvedType.allMethods.forEach { m -> logger.lifecycle("  m:" + m.name) }
          } else {
            if (logger.isDebugEnabled) {
              logger.debug("No completion for " + it.javaClass)
            }
          }
        } catch (e: UnsupportedOperationException) {
          if ("CorrespondingDeclaration not available for unsolved symbol.".equals(e.message)) {
            logger.lifecycle("Unsolved symbol on line {} and column {}", line, column)
            // TODO somehow signal the caller that there is a compilation issue
          } else {
            logger.error("not supported type found (u) >>>>> " + it.javaClass + " <<<<<<< " + it + " thrown with " + e, e)
          }
        } catch (e: Exception) {
          logger.error("not supported type found >>>>> " + it.javaClass + " <<<<<<< " + it + " thrown with " + e, e)
        }
      }
    } else {
      logger.lifecycle("not able to create java parser facade")
    }
  }
}
