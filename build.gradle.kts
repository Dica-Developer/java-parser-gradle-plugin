import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  `java-gradle-plugin`
  `kotlin-dsl`
  `eclipse`
  `idea`
  `groovy`
}

java {
  sourceCompatibility = JavaVersion.VERSION_11
  targetCompatibility = JavaVersion.VERSION_11
}

group = "org.dica-developer"
description = "Plugin to create an AST out of the configured java source."
version = "1.0.0"

dependencies {
  implementation("com.github.javaparser:javaparser-core:3.15.7")
  implementation("com.github.javaparser:javaparser-symbol-solver-core:3.15.7")
}

gradlePlugin {
  plugins {
    create("java-parser-gradle-plugin") {
      id = "java-parser-gradle-plugin"
      implementationClass = "plugin.MyPlugin"
    }
  }
}

repositories {
  mavenCentral()
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    jvmTarget = "1.8"
  }
}

// cp ../../workspace.private/java-parser-gradle-plugin/build/libs/java-parser-gradle-plugin-1.0.0.jar /home/ms/workspace.main/dap/lib/build/gradle/
// build.gradle -> apply plugin: 'java-parser-gradle-plugin'
// buildSrc/build.gradle ->
//dependencies {
//    compile 'com.github.javaparser:javaparser-core:3.6.16'
//    compile 'com.github.javaparser:java-symbol-solver-core:3.6.16'
//    compile 'org.dica-developer:gradle-java-parser:1.0.0'
